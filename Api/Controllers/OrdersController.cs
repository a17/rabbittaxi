﻿using System;
using System.Text;
using Microsoft.AspNetCore.Mvc;
using Model;
using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace Api.Controllers
{
    [Route("api/[controller]")]
    public class OrdersController : Controller
    {
        [HttpPost]
        public ActionResult Post([FromBody]Order order)
        {
            var factory = new ConnectionFactory
            {
                HostName = "localhost",
                Port = 8687
            };

            int price = 0;

            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                var orderJson = JsonConvert.SerializeObject(order);

                var orderBytes = Encoding.UTF8.GetBytes(orderJson);

                PublishLogs(channel, orderBytes);

                price = RequestPrice(channel, orderBytes);
            }

            return Json(new { Result = $"Order placed. Price is ${price}" });
        }

        private static void PublishLogs(IModel channel, byte[] orderBytes)
        {
            channel.ExchangeDeclare(
                exchange: "logs",
                type: "fanout");

            channel.BasicPublish(
                exchange: "logs",
                routingKey: "",
                basicProperties: null,
                body: orderBytes);
        }

        private static int RequestPrice(IModel channel, byte[] orderBytes)
        {
            var exchangeName = "calculatorExchange";

            channel.ExchangeDeclare(
                exchange: exchangeName,
                type: "direct");

            var replyQueueName = channel.QueueDeclare().QueueName;

            channel.QueueBind(
                queue: replyQueueName,
                exchange: exchangeName,
                routingKey: replyQueueName);

            var consumer = new QueueingBasicConsumer(channel);

            channel.BasicConsume(
                queue: replyQueueName,
                noAck: true,
                consumer: consumer);

            var correlationId = Guid.NewGuid().ToString();

            var properties = channel.CreateBasicProperties();
            properties.ReplyTo = replyQueueName;
            properties.CorrelationId = correlationId;

            channel.BasicPublish(
                exchange: exchangeName,
                routingKey: "priceRequest",
                basicProperties: properties,
                body: orderBytes);

            while (true)
            {
                var eventArgs = consumer.Queue.Dequeue();

                if (eventArgs.BasicProperties.CorrelationId == correlationId)
                {
                    return int.Parse(Encoding.UTF8.GetString(eventArgs.Body));
                }
            }
        }
    }
}
