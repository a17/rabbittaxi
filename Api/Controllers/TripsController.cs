﻿using System.Text;
using Microsoft.AspNetCore.Mvc;
using Model;
using Newtonsoft.Json;
using RabbitMQ.Client;

namespace Api.Controllers
{
    [Route("api/[controller]")]
    public class TripsController : Controller
    {
        [HttpPost]
        public ActionResult Post([FromBody]Trip trip)
        {
            var factory = new ConnectionFactory
            {
                HostName = "localhost",
                Port = 8687
            };

            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                var queueName = "statisticsCalculator";

                channel.QueueDeclare(
                    queue: queueName,
                    durable: true,
                    exclusive: false,
                    autoDelete: false,
                    arguments: null);

                var tripJson = JsonConvert.SerializeObject(trip);

                var tripBytes = Encoding.UTF8.GetBytes(tripJson);

                channel.BasicPublish(
                    exchange: "",
                    routingKey: queueName,
                    basicProperties: null,
                    body: tripBytes);
            }

            return Json(new { Result = "Trip finished" });
        }
    }
}