﻿using System;
using System.Text;
using Model;
using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace StatisticsCalculator
{
    class Program
    {
        static void Main(string[] args)
        {
            var factory = new ConnectionFactory
            {
                HostName = "localhost",
                Port = 8687
            };

            using (var connection = factory.CreateConnection())
            {
                using (var channel = connection.CreateModel())
                {
                    var queueName = "statisticsCalculator";

                    channel.QueueDeclare(
                        queue: queueName,
                        durable: true,
                        exclusive: false,
                        autoDelete: false,
                        arguments: null);

                    var consumer = new EventingBasicConsumer(channel);

                    consumer.Received += (model, ea) =>
                    {
                        var tripJson = Encoding.UTF8.GetString(ea.Body);

                        var trip = JsonConvert.DeserializeObject<Trip>(tripJson);

                        Console.WriteLine($"Statistics for {trip} calculated");
                    };

                    channel.BasicConsume(
                        queue: queueName,
                        noAck: true,
                        consumer: consumer);

                    Console.WriteLine("Waiting for commands...");

                    Console.ReadLine();
                }
            }
        }
    }
}