﻿using System;
using System.Text;
using Model;
using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace DatabaseLogger
{
    class Program
    {
        static void Main(string[] args)
        {
            var factory = new ConnectionFactory
            {
                HostName = "localhost",
                Port = 8687
            };

            using (var connection = factory.CreateConnection())
            {
                using (var channel = connection.CreateModel())
                {
                    var queueName = "databaseLogs";

                    channel.ExchangeDeclare(
                        exchange: "logs",
                        type: "fanout");

                    channel.QueueDeclare(
                        queue: queueName,
                        durable: false,
                        exclusive: false,
                        autoDelete: false,
                        arguments: null);

                    channel.QueueBind(
                        queue: queueName,
                        exchange: "logs",
                        routingKey: "");

                    var consumer = new EventingBasicConsumer(channel);

                    consumer.Received += (model, ea) =>
                    {
                        var orderJson = Encoding.UTF8.GetString(ea.Body);

                        var order = JsonConvert.DeserializeObject<Order>(orderJson);

                        Console.WriteLine($"{order} logged to the databse");
                    };

                    channel.BasicConsume(
                        queue: queueName,
                        noAck: true,
                        consumer: consumer);

                    Console.WriteLine("Waiting for events...");

                    Console.ReadLine();
                }
            }
        }
    }
}