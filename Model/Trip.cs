﻿namespace Model
{
    public class Trip
    {
        public double MileageInKm { get; set; }

        public int TimeInSeconds { get; set; }

        public override string ToString()
        {
            return $"Trip with {MileageInKm} km and {TimeInSeconds} time";
        }
    }
}