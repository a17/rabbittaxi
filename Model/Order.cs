﻿namespace Model
{
    public class Order
    {
        public string Departure { get; set; }

        public string Destination { get; set; }

        public override string ToString()
        {
            return $"Order from {Departure} to {Destination}";
        }
    }
}