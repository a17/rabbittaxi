﻿using System;
using System.Text;
using Model;
using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace AuditTool
{
    class Program
    {
        static void Main(string[] args)
        {
            var factory = new ConnectionFactory
            {
                HostName = "localhost",
                Port = 8687
            };

            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                var exchangeName = "calculatorExchange";

                channel.ExchangeDeclare(
                    exchange: exchangeName,
                    type: "direct");

                var requestQueueName = "auditRequests";

                channel.QueueDeclare(
                    queue: requestQueueName,
                    durable: false,
                    exclusive: true,
                    autoDelete: true);

                channel.QueueBind(requestQueueName, exchangeName, "priceRequest");

                var consumer = new EventingBasicConsumer(channel);

                consumer.Received += (model, ea) =>
                {
                    var orderJson = Encoding.UTF8.GetString(ea.Body);

                    var order = JsonConvert.DeserializeObject<Order>(orderJson);

                    Console.WriteLine($"{order}. Request received at {DateTime.Now}");
                };

                channel.BasicConsume(
                    queue: requestQueueName,
                    noAck: true,
                    consumer: consumer);

                Console.WriteLine("Auditing requests...");

                Console.ReadLine();
            }
        }
    }
}