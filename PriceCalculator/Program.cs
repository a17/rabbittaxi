﻿using System;
using System.Text;
using Model;
using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace PriceCalculator
{
    class Program
    {
        static void Main(string[] args)
        {
            var factory = new ConnectionFactory
            {
                HostName = "localhost",
                Port = 8687
            };

            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                var exchangeName = "calculatorExchange";

                channel.ExchangeDeclare(
                    exchange: exchangeName,
                    type: "direct");

                var queueName = "priceCalculator";

                channel.QueueDeclare(
                    queue: queueName,
                    durable: false,
                    exclusive: false,
                    autoDelete: false,
                    arguments: null);

                channel.QueueBind(
                    queue: queueName,
                    exchange: exchangeName,
                    routingKey: "priceRequest");

                channel.BasicQos(0, 1, false);

                var consumer = new EventingBasicConsumer(channel);

                channel.BasicConsume(
                    queue: queueName,
                    noAck: false,
                    consumer: consumer);

                Console.WriteLine("Waiting for requests...");

                consumer.Received += (model, ea) =>
                {
                    var body = ea.Body;
                    var properties = ea.BasicProperties;

                    var replyProperties = channel.CreateBasicProperties();
                    replyProperties.CorrelationId = properties.CorrelationId;

                    var orderJson = Encoding.UTF8.GetString(body);
                    var order = JsonConvert.DeserializeObject<Order>(orderJson);
                    
                    var price = new Random(Environment.TickCount).Next(130);

                    Console.WriteLine($"Calculated price for {order} - ${price}");

                    var responseBytes = Encoding.UTF8.GetBytes(price.ToString());

                    channel.BasicPublish(
                        exchange: exchangeName,
                        routingKey: properties.ReplyTo,
                        basicProperties: replyProperties,
                        body: responseBytes);

                    channel.BasicAck(
                        deliveryTag: ea.DeliveryTag,
                        multiple: false);
                };

                Console.ReadLine();
            }
        }
    }
}